package co.com.areamovil.mvp_clean_architecture.di;

import android.app.Application;
import android.arch.persistence.room.Room;
import co.com.areamovil.mvp_clean_architecture.api.ApiService;
import co.com.areamovil.mvp_clean_architecture.db.AppDataBase;
import co.com.areamovil.mvp_clean_architecture.db.UserDao;
import co.com.areamovil.mvp_clean_architecture.util.LiveDataCallAdapterFactory;
import dagger.Module;
import dagger.Provides;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class) class AppModule {

  @Provides @Singleton HttpLoggingInterceptor provideHttpLoggingInterceptor() {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return interceptor;
  }

  @Provides @Singleton OkHttpClient provideOkHttpClient(
      HttpLoggingInterceptor httpLoggingInterceptor) {
    List<Protocol> protocols = new ArrayList<>();
    protocols.add(Protocol.HTTP_1_1);
    OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
    okHttpClient.connectTimeout(1, TimeUnit.MINUTES);
    okHttpClient.readTimeout(1, TimeUnit.MINUTES);
    okHttpClient.protocols(protocols);
    okHttpClient.addInterceptor(httpLoggingInterceptor);
    return okHttpClient.build();
  }

  @Singleton @Provides public ApiService provideApiService(OkHttpClient okHttpClient) {
    return new Retrofit.Builder().baseUrl("https://api.github.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addCallAdapterFactory(new LiveDataCallAdapterFactory())
        .client(okHttpClient)
        .build()
        .create(ApiService.class);
  }

  @Singleton @Provides public AppDataBase provideDb(Application app) {
    return Room.databaseBuilder(app, AppDataBase.class, "local.db").build();
  }

  @Singleton @Provides public UserDao provideUserDao(AppDataBase db) {
    return db.userDao();
  }

  //TODO: add more provides Dao for each ModelDao
}
