package co.com.areamovil.mvp_clean_architecture.ui.user;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import co.com.areamovil.mvp_clean_architecture.R;
import co.com.areamovil.mvp_clean_architecture.binding.FragmentDataBindingComponent;
import co.com.areamovil.mvp_clean_architecture.databinding.UserBinding;
import co.com.areamovil.mvp_clean_architecture.di.Injectable;
import co.com.areamovil.mvp_clean_architecture.ui.detail.Detail;
import co.com.areamovil.mvp_clean_architecture.util.AutoClearedValue;
import javax.inject.Inject;

/**
 * Created by AreaMovil on 8/08/17.
 */

public class UserFragment extends LifecycleFragment implements Injectable {

  @Inject ViewModelProvider.Factory viewModelFactory;
  public DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

  private UserViewModel userViewModel;
  private AutoClearedValue<UserBinding> binding;

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.e(this.getClass().getName(), "onCreate");
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    UserBinding dataBinding =
        DataBindingUtil.inflate(inflater, R.layout.user, container, false, dataBindingComponent);
    dataBinding.setRetryCallback(() -> userViewModel.retry());
    binding = new AutoClearedValue<>(this, dataBinding);
    return dataBinding.getRoot();
  }

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
    userViewModel.setLogin("mikel063093");
    userViewModel.getUser().observe(this, userResource -> {
      binding.get().setUser(userResource == null ? null : userResource.data);
      binding.get().setUserResource(userResource);
      binding.get().avatar.setOnClickListener(view -> {
        getActivity().startActivity(new Intent(getContext(), Detail.class));
      });
      // this is only necessary because espresso cannot read data binding callbacks.
      binding.get().executePendingBindings();
    });
  }
}
