package co.com.areamovil.mvp_clean_architecture.ui.user;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.VisibleForTesting;
import co.com.areamovil.mvp_clean_architecture.model.Resource;
import co.com.areamovil.mvp_clean_architecture.model.User;
import co.com.areamovil.mvp_clean_architecture.util.AbsentLiveData;
import co.com.areamovil.mvp_clean_architecture.util.Objects;
import javax.inject.Inject;

/**
 * Created by AreaMovil on 8/08/17.
 */

public class UserViewModel extends ViewModel {
  @VisibleForTesting private MutableLiveData<String> login = new MutableLiveData<>();

  private final LiveData<Resource<User>> user;

  @Inject UserViewModel(final UserRepository userRepository) {

    user = Transformations.switchMap(login, login -> {
      if (login == null) {
        return AbsentLiveData.create();
      } else {
        return userRepository.loadUser(login);
      }
    });
  }

  public void setLogin(String login) {
    if (Objects.equals(this.login.getValue(), login)) {
      return;
    }
    this.login.setValue(login);
  }

  public LiveData<Resource<User>> getUser() {
    return user;
  }

  public void retry() {
    if (this.login.getValue() != null) {
      this.login.setValue(this.login.getValue());
    }
  }
}
