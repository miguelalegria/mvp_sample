package co.com.areamovil.mvp_clean_architecture.di;

import android.app.Application;
import co.com.areamovil.mvp_clean_architecture.AppMain;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import javax.inject.Singleton;

/**
 * Created by AreaMovil on 8/08/17.
 */

@Singleton
@Component(modules = {
      AppModule.class,
      AndroidInjectionModule.class,
      ActivityBuilderModule.class
}) public interface AppComponent {
  @Component.Builder
    interface Builder {
    @BindsInstance
    Builder application(Application application);

    AppComponent build();
  }

  void inject(AppMain app);
}
