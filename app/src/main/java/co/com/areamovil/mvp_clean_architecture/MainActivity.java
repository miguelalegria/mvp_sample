package co.com.areamovil.mvp_clean_architecture;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import co.com.areamovil.mvp_clean_architecture.databinding.ActivityMainBinding;
import co.com.areamovil.mvp_clean_architecture.ui.user.UserFragment;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import javax.inject.Inject;

public class MainActivity extends AppCompatActivity
    implements LifecycleOwner, HasSupportFragmentInjector {

  @Inject DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.e(this.getClass().getName(), "onCreate");
    ActivityMainBinding activityMainBinding =
        DataBindingUtil.setContentView(this, R.layout.activity_main);
    FragmentManager fragmentManager = getSupportFragmentManager();
    if (savedInstanceState == null) {
      fragmentManager.beginTransaction()
          .replace(R.id.container1, new UserFragment())
          .commitAllowingStateLoss();
    }
  }

  @Override protected void onPause() {
    super.onPause();
    Log.e(this.getClass().getName(), "onPause");
  }

  @Override protected void onStop() {
    super.onStop();
    Log.e(this.getClass().getName(), "onStop");
  }

  @Override protected void onResume() {
    super.onResume();
    Log.e(this.getClass().getName(), "onResume");
  }

  @Override public AndroidInjector<Fragment> supportFragmentInjector() {
    return dispatchingAndroidInjector;
  }

  private LifecycleRegistry registry = new LifecycleRegistry(this);

  @Override public LifecycleRegistry getLifecycle() {
    return registry;
  }
}
