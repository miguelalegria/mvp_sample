package co.com.areamovil.mvp_clean_architecture.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AreaMovil on 8/08/17.
 */
@Entity(tableName = "User") public class User {

  @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) public int id = 0;
  @SerializedName("avatar_url") @ColumnInfo(name = "avatar_url") public String avatarUrl;
  @ColumnInfo(name = "login") public String login;
  @ColumnInfo(name = "name") public String name;
}
