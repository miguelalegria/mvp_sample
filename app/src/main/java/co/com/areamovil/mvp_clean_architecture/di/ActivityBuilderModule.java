package co.com.areamovil.mvp_clean_architecture.di;

import co.com.areamovil.mvp_clean_architecture.MainActivity;
import co.com.areamovil.mvp_clean_architecture.ui.detail.Detail;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

  @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
  abstract MainActivity mainActivity();

  @ContributesAndroidInjector
  abstract Detail detail();
}

