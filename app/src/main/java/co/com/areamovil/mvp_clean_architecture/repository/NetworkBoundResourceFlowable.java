package co.com.areamovil.mvp_clean_architecture.repository;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import co.com.areamovil.mvp_clean_architecture.AppExecutors;
import co.com.areamovil.mvp_clean_architecture.api.ApiResponse;
import co.com.areamovil.mvp_clean_architecture.model.Resource;
import io.reactivex.Flowable;
import org.reactivestreams.Subscriber;

/**
 * Created by AreaMovil on 8/08/17.
 */

public abstract class NetworkBoundResourceFlowable<ResultType, RequestType> {
  private final AppExecutors appExecutors;
  private Flowable<Resource<ResultType>> outResult = null;
  private Resource<ResultType> result = Resource.loading(null);

  @MainThread public NetworkBoundResourceFlowable(AppExecutors appExecutors) {
    this.appExecutors = appExecutors;
    Flowable<ResultType> dbSource = loadFromDb();
    dbSource.subscribe((ResultType resultType) -> {
      if (shouldFetch(resultType)) {
        fetchFromNetwork(dbSource);
      } else {
        outResult = new Flowable<Resource<ResultType>>() {
          @Override protected void subscribeActual(Subscriber<? super Resource<ResultType>> s) {
            s.onNext(Resource.success(resultType));
            s.onComplete();
          }
        };
      }
    }, Throwable::printStackTrace);
  }

  private void fetchFromNetwork(final Flowable<ResultType> dbSource) {
    Flowable<ApiResponse<RequestType>> apiResponse = createCall();
    apiResponse.subscribe(requestTypeApiResponse -> {
      if (requestTypeApiResponse.isSuccessful()) {
        appExecutors.diskIO().execute(() -> {
          saveCallResult(processResponse(requestTypeApiResponse));
          appExecutors.mainThread().execute(() -> outResult = new Flowable<Resource<ResultType>>() {
            @Override protected void subscribeActual(Subscriber<? super Resource<ResultType>> s) {
              Flowable<ResultType> _dbSource = loadFromDb();
              _dbSource.subscribe(resultType -> s.onNext(Resource.success(resultType)));
              s.onComplete();
            }
          });
        });
      } else {
        onFetchFailed();
        outResult = new Flowable<Resource<ResultType>>() {
          @Override protected void subscribeActual(Subscriber<? super Resource<ResultType>> s) {
            dbSource.subscribe(resultType -> s.onNext(Resource.success(resultType)));
            s.onComplete();
          }
        };
      }
    });
  }

  @WorkerThread protected RequestType processResponse(ApiResponse<RequestType> response) {
    return response.body;
  }

  protected void onFetchFailed() {
  }

  public Flowable<Resource<ResultType>> asFlowable() {
    return outResult;
  }

  @WorkerThread protected abstract void saveCallResult(@NonNull RequestType item);

  @MainThread protected abstract boolean shouldFetch(@Nullable ResultType data);

  @NonNull @MainThread protected abstract Flowable<ResultType> loadFromDb();

  @NonNull @MainThread protected abstract Flowable<ApiResponse<RequestType>> createCall();
}
