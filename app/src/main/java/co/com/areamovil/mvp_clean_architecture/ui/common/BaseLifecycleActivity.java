package co.com.areamovil.mvp_clean_architecture.ui.common;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by AreaMovil on 9/08/17.
 */

public abstract class BaseLifecycleActivity extends AppCompatActivity implements LifecycleOwner {
  private LifecycleRegistry registry = new LifecycleRegistry(this);

  @Override public LifecycleRegistry getLifecycle() {
    return registry;
  }

  public abstract int getLayoutId();
}
