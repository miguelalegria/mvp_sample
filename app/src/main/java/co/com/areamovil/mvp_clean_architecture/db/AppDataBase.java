package co.com.areamovil.mvp_clean_architecture.db;

/**
 * Created by AreaMovil on 8/08/17.
 */

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import co.com.areamovil.mvp_clean_architecture.model.User;

@Database(entities = { User.class }, version = 1) public abstract class AppDataBase
    extends RoomDatabase {
  public abstract UserDao userDao();
}
