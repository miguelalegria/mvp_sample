package co.com.areamovil.mvp_clean_architecture.ui.detail;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import co.com.areamovil.mvp_clean_architecture.R;
import co.com.areamovil.mvp_clean_architecture.databinding.ActivityDetailBinding;
import co.com.areamovil.mvp_clean_architecture.ui.user.UserViewModel;
import dagger.android.AndroidInjection;
import javax.inject.Inject;

public class Detail extends AppCompatActivity implements LifecycleRegistryOwner {

  @Inject UserViewModel userViewModel;

  LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

  @Override protected void onCreate(Bundle savedInstanceState) {
    Log.e(this.getClass().getName(), "onCreate");
    AndroidInjection.inject(this);
    super.onCreate(savedInstanceState);
    ActivityDetailBinding activityDetailBinding =
        DataBindingUtil.setContentView(this, R.layout.activity_detail);
    activityDetailBinding.setRetryCallback(() -> userViewModel.retry());

    userViewModel.setLogin("googlesamples");
    userViewModel.getUser().observe(this, userResource -> {
      activityDetailBinding.setUser(userResource == null ? null : userResource.data);
      activityDetailBinding.setUserResource(userResource);
      activityDetailBinding.executePendingBindings();
    });
  }

  @Override public LifecycleRegistry getLifecycle() {
    return lifecycleRegistry;
  }
}
