package co.com.areamovil.mvp_clean_architecture.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import co.com.areamovil.mvp_clean_architecture.ui.detail.DetailViewModel;
import co.com.areamovil.mvp_clean_architecture.ui.user.UserViewModel;
import co.com.areamovil.mvp_clean_architecture.util.ViewModelFactory;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module public abstract class ViewModelModule {

  @Binds @IntoMap @ViewModelKey(UserViewModel.class)
  abstract ViewModel bindUserViewModel(UserViewModel userViewModel);

  @Binds @IntoMap @ViewModelKey(DetailViewModel.class)
  abstract ViewModel bindDetailViewModel(DetailViewModel userViewModel);

  //TODO: add more viewModels here

  @Binds abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
