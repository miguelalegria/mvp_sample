package co.com.areamovil.mvp_clean_architecture.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import co.com.areamovil.mvp_clean_architecture.model.User;
import io.reactivex.Flowable;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by AreaMovil on 8/08/17.
 */

@Dao public interface UserDao {
  @Query("select * from User") Flowable<List<User>> getAllUsers();

  @Query("select * from User where id= :id") Flowable<User> findUserById(int id);

  @Query("SELECT * FROM user WHERE login = :login") LiveData<User> findUserByLogin(String login);

  @Insert(onConflict = REPLACE) void insertUser(User user);

  @Update(onConflict = REPLACE) void updateUser(User user);

  @Delete void deleteuser(User user);
}

