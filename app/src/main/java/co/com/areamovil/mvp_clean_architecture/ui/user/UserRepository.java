package co.com.areamovil.mvp_clean_architecture.ui.user;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import co.com.areamovil.mvp_clean_architecture.api.ApiService;
import co.com.areamovil.mvp_clean_architecture.db.UserDao;
import co.com.areamovil.mvp_clean_architecture.model.Resource;
import co.com.areamovil.mvp_clean_architecture.model.User;
import co.com.areamovil.mvp_clean_architecture.repository.NetworkBoundResource;
import javax.inject.Inject;
import javax.inject.Singleton;
import retrofit2.Call;

/**
 * Created by AreaMovil on 8/08/17.
 */
@Singleton public class UserRepository {
  private final UserDao userDao;
  private final ApiService githubService;


  @Inject
  public UserRepository(UserDao userDao, ApiService githubService) {
    this.userDao = userDao;
    this.githubService = githubService;
  }

  public LiveData<Resource<User>> loadUser(String login) {

    return new NetworkBoundResource<User, User>() {
      @Override protected void saveCallResult(@NonNull User item) {
        userDao.insertUser(item);
      }

      @NonNull @Override protected LiveData<User> loadFromDb() {
        return userDao.findUserByLogin(login);
      }

      @NonNull @Override protected Call<User> createCall() {
        return githubService.getUser(login);
      }
    }.getAsLiveData();
  }
}
