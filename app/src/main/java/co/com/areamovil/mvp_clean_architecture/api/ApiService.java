package co.com.areamovil.mvp_clean_architecture.api;

import android.arch.lifecycle.LiveData;
import co.com.areamovil.mvp_clean_architecture.model.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by AreaMovil on 8/08/17.
 */

public interface ApiService {

  @GET("users/{login}") Call<User> getUser(@Path("login") String login);
}
